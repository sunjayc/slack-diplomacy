#!/usr/bin/env python2

from __future__ import print_function

from slacker import Slacker
import re
from collections import defaultdict as ddict

with open('slack_auth') as f:
    client_id = f.readline().strip()
    client_secret = f.readline().strip()
    token = f.readline().strip()
    diplo = f.readline().strip() # the id for the channel

slack = Slacker(token)

def is_hash(text):
    return bool(re.match(r'^[0-9a-f]{64}$', text))

def is_msg(text):
    return bool(re.search(r'0x[0-9A-F]{16}:::.*:::[0-9a-f]{64}', text, flags=re.S))

def unpack_msg(text):
    m = re.search(r'0x[0-9A-F]{16}:::.*:::[0-9a-f]{64}', text, flags=re.S)
    return m.group(0)

if __name__ == '__main__':
    import sys
    more = True
    oldest = 0
    do_more = False
    if len(sys.argv) > 1:
        oldest = sys.argv[1]
        do_more = True
    latest = 0
    while more:
        r = slack.groups.history(diplo, latest=latest, oldest=oldest)
        more = r.body['has_more']
        x = r.body['messages']
        if not x:
            break
        x.reverse()
        latest = x[0]['ts']
        hashes = ddict(list)
        for m in x:
            if 'text' not in m:
                continue
            if 'user' not in m:
                continue
            text = m['text'].strip()
            user = m['user']
            ts = m['ts']
            if is_hash(text):
                print(ts + ' ' + user + ' ' + text)
            if is_msg(text):
                print(ts + ' ' + user + ' ', end='')
                print(unpack_msg(text).encode('utf-8'))
        if not do_more:
            break
