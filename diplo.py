#!/usr/bin/env python2

from __future__ import print_function

import re
import time
from collections import defaultdict as ddict
from commitment import check_hash

from HTMLParser import HTMLParser
html = HTMLParser()

# the 'players' file should look something like this:
# U2NKJUH8U:Sunjay / Britain        
# U0DCPMW14:David  / France         
# U2NK1FE5P:Brian  / Austria-Hungary
# U0DCDAJ5V:Joe    / Germany        
# U2PHR3RQF:Louis  / Italy          
# U2NJB9989:Dustin / Turkey         
# U2PJ8SG8K:Gary   / Russia         
# (but without the '# 's of course)
# (the trailing spaces are intentional)
names = {}
with open('players') as f:
    for line in f:
        uid,name = line[:-1].split(':', 1)
        names[uid] = name

def is_hash(text):
    return bool(re.match(r'^[0-9a-f]{64}$', text))

def is_msg(text):
    return bool(re.match(r'^0x[0-9A-F]{16}:::.*:::[0-9a-f]{64}$', text, flags=re.S))

def unpack_msg(text):
    m = re.match(r'^0x[0-9A-F]{16}:::(.*):::([0-9a-f]{64})$', text, flags=re.S)
    return m.group(2, 1)

import sys
COMPACT = True
FULL = False
ALL_HASHES = False
if '--full' in sys.argv:
    COMPACT = False
    FULL = True
if '--all' in sys.argv:
    COMPACT = False
    ALL_HASHES = True
    FULL = True
if '--compact' in sys.argv:
    COMPACT = True

to_print = []
hashes = ddict(list)
with open('hashes') as f:
    running_buf = ''
    for m in f:
        running_buf += m
        ts,user,text = running_buf[:-1].split(' ', 2)
        lts = time.localtime(float(ts))
        fts = time.strftime('%m/%d/%y %I:%M:%S %p', lts)
        if is_hash(text):
            running_buf = ''
            hashes[user].append((lts,text,False))
        elif is_msg(text):
            running_buf = ''
            text = html.unescape(text)
            h, msg = unpack_msg(text)
            if hashes[user] and hashes[user][-1][1] == h:
                hlts,htext,_ = hashes[user][-1]
                hashes[user][-1] = (hlts,htext,True)
                good = check_hash(text)
                to_print.append((lts,good,names[user],msg))

for user in hashes:
    user_hashes = hashes[user]
    if ALL_HASHES:
        for lts,_,marked in user_hashes:
            to_print.append((lts,names[user]))
    else:
        lts,_,marked = user_hashes[-1]
        if not marked:
            to_print.append((lts,names[user]))

to_print.sort(key=lambda x: x[0])

final_yday = to_print[-1][0].tm_yday

last_yday = -1
for line in to_print:
    lts = line[0]

    if not FULL:
        if len(line) == 4 and lts.tm_yday != final_yday:
            continue

    if lts.tm_yday != last_yday:
        if not COMPACT and last_yday != -1:
            print()
        last_yday = lts.tm_yday
    fts = time.strftime('%m/%d/%y %I:%M:%S %p', lts)

    if len(line) == 2:
        _,user = line
        print(fts + '     ' + user)
    elif len(line) == 4:
        _,good,user,msg = line
        good = '     ' if good else ' [!] '
        msg = msg.replace('\n', '\\n')
        print(fts + good + user + ' : ' + msg)
    else:
        pass
