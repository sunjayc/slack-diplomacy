#!/bin/bash
if [[ $# -eq 0 ]]
then
  tail -n2 ~/.commitments/statements.log | head -c -1 | tee /dev/tty | head -c -1 | xclip -sel clip
elif [[ "$1" = "--hash" ]]
then
  tail -n2 ~/.commitments/statements.log | head -c -1 | awk -F':::' '{print $2 "\n" $3}' | tee /dev/tty | tail -n1 | head -c -1 | xclip -sel clip
  echo "Copied just the hash"
else
  echo "Usage: $0 [--hash]" >&2
fi
