#!/usr/bin/env python2

from __future__ import print_function

import re

def order(line):
    prefix = ': '
    line = line.strip()
    if prefix in line:
        line = line[line.find(prefix)+len(prefix):]
    line = line.replace('->', '-')
    line = line.replace('Convoys ', 'C')
    line = line.replace('Supports ', 'S')
    line = line.replace('Holds', 'hold')
    return line

def reverse(line):
    prefix = ' : '
    line = line.strip()
    if prefix in line:
        line = line[line.find(prefix)+len(prefix):]
        line = re.sub(r'-.?holds?', r' holds', line, flags=re.I)
        line = re.sub(r'[;.,]|\\n',r'\n', line)
        line = line.strip()
        return line + '\n'
    return ''

if __name__ == '__main__':
    import sys
    if '--rev' in sys.argv:
        for line in sys.stdin:
            print(reverse(line))
    else:
        output = []
        committing = '--commit' in sys.argv
        for line in sys.stdin.readlines():
            line = order(line)
            output.append(line)
            if not committing:
                print(line)
        if committing:
            from commitment import do_statement
            orders = ' ; '.join(output)
            do_statement(orders)

